package ar.edu.unlu.poo.biblioteca.controladores;

import ar.edu.unlu.poo.biblioteca.modelos.Libro;
import ar.edu.unlu.poo.biblioteca.modelos.ModeloLibros;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoEncontrado;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoDisponible;
import ar.edu.unlu.poo.biblioteca.vistas.IVista;

import java.util.ArrayList;
import java.util.List;

public class Controlador {
    private final IVista vista;
    private ModeloLibros modelo;

    public Controlador(IVista vista) {
        this.vista = vista;
        vista.setControlador(this);
    }
    public void setModelo(ModeloLibros modelo) {
        this.modelo = modelo;
    }

    public void agregarLibro(String titulo, String autor, int ejemplares) {
        modelo.agregarLibro(new Libro(titulo, autor, ejemplares));
    }

    public void prestarLibro(int id) throws LibroNoDisponible, LibroNoEncontrado {
        modelo.prestarLibro(id);
    }

    public void devolverLibro(int id) throws LibroNoEncontrado, LibroNoDisponible {
        modelo.devolverLibro(id);
    }

    public List<Libro> obtenerTodosLosLibros() {
        return modelo.obtenerTodosLosLibros();
    }

    public Libro obtenerLibroPorId(int id) {
        return modelo.obtenerLibroPorId(id);
    }

    public List<Libro> buscarLibrosPorTitulo(String parteDelTitulo) {
        return modelo.buscarLibrosPorTitulo(parteDelTitulo);
    }
}
