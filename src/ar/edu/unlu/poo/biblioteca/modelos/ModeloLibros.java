package ar.edu.unlu.poo.biblioteca.modelos;

import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoEncontrado;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoDisponible;

import java.util.ArrayList;
import java.util.List;

public class ModeloLibros {
    private List<Libro> listaDeLibros;

    public ModeloLibros() {
        listaDeLibros = new ArrayList<>();
    }

    public List<Libro> obtenerTodosLosLibros() {
        return listaDeLibros;
    }

    public Libro obtenerLibroPorId(int id) {
        for (Libro libro : listaDeLibros) {
            if (libro.getId() == id) {
                return libro;
            }
        }
        return null; // Devolver null si no se encuentra el libro
    }

    public Libro agregarLibro(Libro libro) {
        listaDeLibros.add(libro);
        return libro;
    }

    public List<Libro> buscarLibrosPorTitulo(String parteDelTitulo) {
        List<Libro> librosEncontrados = new ArrayList<>();

        for (Libro libro : listaDeLibros) {
            if (libro.getTitulo().toLowerCase().contains(parteDelTitulo.toLowerCase())) {
                librosEncontrados.add(libro);
            }
        }

        return librosEncontrados;
    }

    public void prestarLibro(int id) throws LibroNoEncontrado, LibroNoDisponible {
        Libro libro = obtenerLibroPorId(id);
        if (libro == null)
            throw new LibroNoEncontrado();
        if (!libro.estaDisponible())
            throw new LibroNoDisponible();
        libro.prestar();
    }

    public void devolverLibro(int id) throws LibroNoEncontrado, LibroNoDisponible {
        Libro libro = obtenerLibroPorId(id);
        if (libro == null)
            throw new LibroNoEncontrado();
        if (!libro.hayPrestados())
            throw new LibroNoDisponible();
        libro.devolver();
    }

    // Puedes agregar más métodos según las operaciones necesarias, como buscar, prestar, devolver, etc.
}
