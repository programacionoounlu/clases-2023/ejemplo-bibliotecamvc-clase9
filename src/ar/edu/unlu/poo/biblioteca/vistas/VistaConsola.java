package ar.edu.unlu.poo.biblioteca.vistas;

import ar.edu.unlu.poo.biblioteca.controladores.Controlador;
import ar.edu.unlu.poo.biblioteca.modelos.Libro;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoDisponible;
import ar.edu.unlu.poo.biblioteca.modelos.exceptions.LibroNoEncontrado;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VistaConsola implements IVista {

    private Controlador controlador;

    @Override
    public void setControlador(Controlador controlador) {
        this.controlador = controlador;
    }

    private void mostrarMensaje(String mensaje) {
        System.out.println(mensaje);
    }

    @Override
    public void mostrarAltaLibro() {
        Scanner scanner = new Scanner(System.in);
        mostrarMensaje("Ingrese los datos del nuevo libro:");
        System.out.print("Título: ");
        String titulo = scanner.nextLine();
        System.out.print("Autor: ");
        String autor = scanner.nextLine();
        System.out.print("Número de ejemplares disponibles: ");
        int ejemplaresDisponibles = scanner.nextInt();
        controlador.agregarLibro(titulo, autor, ejemplaresDisponibles);
    }

    @Override
    public void mostrarDevolucionLibro() {
        Scanner scanner = new Scanner(System.in);
        mostrarMensaje("Ingrese el ID del libro que desea devolver:");
        int idLibro = scanner.nextInt();
        try {
            controlador.devolverLibro(idLibro);
            mostrarResultadoDevolucionExitosa();
        } catch (LibroNoEncontrado e) {
            mostrarMensaje("El libro con ID " + idLibro + " no existe.");
        } catch (LibroNoDisponible e) {
            mostrarResultadoDevolucionFallida();
        }
    }

    private String solicitarTituloParaBusqueda() {
        Scanner scanner = new Scanner(System.in);
        mostrarMensaje("Ingrese el título que desea buscar: ");
        return scanner.nextLine();
    }

    private String solicitarIdParaBusqueda() {
        Scanner scanner = new Scanner(System.in);
        mostrarMensaje("Ingrese el título que desea buscar: ");
        return scanner.nextLine();
    }

    @Override
    public void mostrarBusquedaLibros() {
        Scanner scanner = new Scanner(System.in);

        // Mostrar el menú de opciones de búsqueda
        mostrarMensaje("Opciones de búsqueda:");
        mostrarMensaje("1. Búsqueda por título");
        mostrarMensaje("2. Búsqueda por autor");  // Agregar otras opciones si es necesario
        mostrarMensaje("3. Mostrar todos los libros");
        mostrarMensaje("4. Busqueda por ID");
        mostrarMensaje("0. Volver al menú anterior");
        mostrarMensaje("Seleccione una opción: ");

        String opcionBusqueda = scanner.nextLine();

        if (opcionBusqueda.equals("1")) {
            // Búsqueda por título
            String titulo = solicitarTituloParaBusqueda();
            List<Libro> librosEncontrados = controlador.buscarLibrosPorTitulo(titulo);

            if (!librosEncontrados.isEmpty()) {
                mostrarResultadosBusqueda(librosEncontrados);
            } else {
                mostrarMensaje("No se encontraron libros con ese título.");
            }
        } else if (opcionBusqueda.equals("2")) {
            // Otras opciones de búsqueda
            // ...
        } else if (opcionBusqueda.equals("3")) {
            List<Libro> todosLosLibros = controlador.obtenerTodosLosLibros();
            mostrarResultadosBusqueda(todosLosLibros);
        }  else if (opcionBusqueda.equals("4")) {
            String id = solicitarIdParaBusqueda();
            Libro libro = controlador.obtenerLibroPorId(Integer.parseInt(id));
            ArrayList<Libro> libros = new ArrayList<>();
            libros.add(libro);
            mostrarResultadosBusqueda(libros);
        } else if (opcionBusqueda.equals("0")) {
            mostrarMensaje("Volviendo al menú anterior.");
        } else {
            mostrarMensaje("Opción de búsqueda no válida.");
        }
    }

    @Override
    public void mostrarPrestamoLibros() {
        Scanner scanner = new Scanner(System.in);
        mostrarMensaje("Ingrese el ID del libro que desea prestar:");
        int idLibro = scanner.nextInt();
        try {
            controlador.prestarLibro(idLibro);
            mostrarResultadoPrestamoExitoso();
        } catch (LibroNoEncontrado e) {
            mostrarMensaje("El libro con ID " + idLibro + " no existe.");
        } catch (LibroNoDisponible e) {
            mostrarResultadoPrestamoFallido();
        }
    }

    @Override
    public void mostrarResultadoPrestamoExitoso() {
        mostrarMensaje("Prestamo exitoso. El libro ha sido prestado.");
    }

    @Override
    public void mostrarResultadoPrestamoFallido() {
        mostrarMensaje("No se pudo realizar el préstamo. El libro no está disponible.");
    }

    @Override
    public void mostrarResultadoDevolucionExitosa() {
        mostrarMensaje("Devolucion exitosa. El libro ha sido devuelto.");
    }

    @Override
    public void mostrarResultadoDevolucionFallida() {
        mostrarMensaje("No se pudo realizar la devolución. El libro no está disponible.");
    }

    @Override
    public void mostrarResultadosBusqueda(List<Libro> librosEncontrados) {
        if (librosEncontrados.isEmpty()) {
            mostrarMensaje("No se encontraron libros con el criterio especificado.");
            return;
        }
        mostrarMensaje("Resultados de la búsqueda: ");
        for (Libro libro: librosEncontrados) {
            mostrarMensaje("ID: " + libro.getId());
            mostrarMensaje("Título: " + libro.getTitulo());
            mostrarMensaje("Autor: " + libro.getAutor());
            mostrarMensaje("Ejemplares disponibles: " + libro.getEjemplaresDisponibles());
            mostrarMensaje("-------------");
        }
    }

    @Override
    public void mostrarMenuPrincipal() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Menú Principal:");
            System.out.println("1. Búsqueda de Libros");
            System.out.println("2. Alta de Libro");
            System.out.println("3. Préstamo de Libro");
            System.out.println("4. Devolución de Libro");
            System.out.println("5. Salir");
            System.out.print("Seleccione una opción: ");

            int opcion = scanner.nextInt();
            scanner.nextLine(); // Consumir la nueva línea

            switch (opcion) {
                case 1:
                    mostrarBusquedaLibros();
                    break;
                case 2:
                    mostrarAltaLibro();
                    break;
                case 3:
                    mostrarPrestamoLibros();
                    break;
                case 4:
                    mostrarDevolucionLibro();
                    break;
                case 5:
                    System.out.println("Gracias por usar el Sistema de Gestión de Biblioteca. ¡Hasta luego!");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Opción no válida. Por favor, elija una opción válida.");
            }
        }
    }
}